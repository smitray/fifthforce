import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

const TextInput = ({
  label,
  fullWidth,
  variant,
  multiline
}) => (
  <>
    <TextField
      label={label}
      margin="normal"
      variant={variant}
      fullWidth={fullWidth}
      multiline={multiline}
    />
  </>
);

TextInput.propTypes = {
  /**
   * Label is used to define the text input
   */
  label: PropTypes.string.isRequired,
  /**
   * Full width is defined to make the text input 100% width
   */
  fullWidth: PropTypes.bool,
  /**
   * Multiline is defined for textarea
   */
  multiline: PropTypes.bool,
  /**
   * Variant is defined for type of Text Input it will be
   */
  variant: PropTypes.string
};

TextInput.defaultProps = {
  fullWidth: false,
  multiline: false,
  variant: 'standard'
};

export default TextInput;
