import {
  TextInput,
  Checkbox,
  Button,
  UploadButton,
  IconButton,
  FabButton
} from 'components';
import { withStyles } from '@material-ui/core';
import { PhotoCamera } from '@material-ui/icons';

const ColorButton = withStyles(() => ({
  root: {
    color: '#fff',
    backgroundColor: '#4caf50',
    '&:hover': {
      backgroundColor: '#388e3c'
    }
  }
}))(Button);

export default () => (
  <>
    <Checkbox
      groupLabel="gender"
      color="secondary"
      data={[{
        label: 'Male',
        value: 'male'
      }, {
        label: 'Female',
        value: 'female'
      }]}
    />
    <TextInput label="Multiline" multiline />
    <ColorButton startIcon={<PhotoCamera />}>
      Hi this is a custom button
    </ColorButton>
    <UploadButton
      fileType="image/*"
      customColor={{ color: 'FF9E1E', hover: 'rgba(255, 158, 31, 0.5)' }}
      icon
    >
      <PhotoCamera />
    </UploadButton>
    <IconButton
      customColor={{ color: 'FF9E1E', hover: 'rgba(255, 158, 31, 0.5)' }}
    >
      <PhotoCamera />
    </IconButton>
    <FabButton customColor={{ color: 'fff', bg: '4caf50', hover: '388e3c' }}>
      <PhotoCamera />
    </FabButton>
  </>
);
